package wordpress

import (
	"context"
	"net/http"
	"replay-api/commons"
	"replay-api/config"

	"github.com/robbiet480/go-wordpress"
	log "github.com/sirupsen/logrus"
)

const (
	seriesURL   = "series"
	podcastsURL = "podcast"
)

var wordpressClientLogger = log.WithField("logger", "wordpress")

//WPClient wrapper for Wordpress client
type WPClient struct {
	client   *wordpress.Client
	sortings map[string]config.Sorting
}

type params struct {
	Page       int    `url:"page,omitempty"`
	PageLength int    `url:"per_page,omitempty"`
	Order      string `url:"order,omitempty"`
	OrderBy    string `url:"orderby,omitempty"`
	Search     string `url:"search,omitempty"`
	SerieIDs   []int  `url:"series,omitempty"`
}

//NewWordpressClient initializes a new Wordpress client
func NewWordpressClient(config *config.Config) (*WPClient, error) {
	wordpressClientLogger.WithField("WordpressURL", config.WordpressConfig.URL).Debugln("Initialize Wordpress client")
	client, err := wordpress.NewClient(config.WordpressConfig.URL, nil)
	if err != nil {
		wordpressClientLogger.WithError(err).Warn("Error initializing Wordpress client")
		return nil, err
	}
	return &WPClient{client, config.WordpressConfig.Sorting}, err
}

//LoadSeries search or load all series if search empty
func (wpClient WPClient) LoadSeries(search string, page, pageLength int) []commons.SerieType {
	returnedSeries := make([]commons.SerieType, 0)
	ctx := context.Background()
	params := params{
		Page:       page,
		PageLength: pageLength,
		Order:      wpClient.sortings[config.SortingSeries].Order,
		OrderBy:    wpClient.sortings[config.SortingSeries].OrderBy,
		Search:     search,
	}
	wordpressClientLogger.WithFields(log.Fields{
		"Page":       params.Page,
		"PageLength": params.PageLength,
		"Order":      params.Order,
		"OrderBy":    params.OrderBy,
		"Search":     params.Search,
	}).Debug("Wordpress series request")
	resp, err := wpClient.client.List(ctx, seriesURL, params, &returnedSeries)
	defer closeBody(resp)

	if resp == nil || resp.StatusCode != http.StatusOK {
		wordpressClientLogger.WithError(err).WithField("Response", resp).Warn("Error while getting series")
	}

	return returnedSeries
}

//LoadPodcasts podcasts search or load all podcasts if search empty
func (wpClient WPClient) LoadPodcasts(search string, serieID, page, pageLength int) []commons.PodcastType {
	returnedPodcasts := make([]commons.PodcastType, 0)
	ctx := context.Background()
	params := params{
		Page:       page,
		PageLength: pageLength,
		Order:      wpClient.sortings[config.SortingPodcast].Order,
		OrderBy:    wpClient.sortings[config.SortingPodcast].OrderBy,
		Search:     search,
		SerieIDs:   []int{serieID},
	}
	wordpressClientLogger.WithFields(log.Fields{
		"Page":       params.Page,
		"PageLength": params.PageLength,
		"Order":      params.Order,
		"OrderBy":    params.OrderBy,
		"Search":     params.Search,
		"SerieIDs":   params.SerieIDs,
	}).Debug("Wordpress podcasts request")
	resp, err := wpClient.client.List(ctx, podcastsURL, params, &returnedPodcasts)
	defer closeBody(resp)

	if resp == nil || resp.StatusCode != http.StatusOK {
		wordpressClientLogger.WithError(err).WithField("Response", resp).Warn("Error while getting podcast")
	}

	return returnedPodcasts
}

func closeBody(resp *wordpress.Response) {
	if resp != nil && resp.Body != nil {
		resp.Body.Close()
	}
}

//GetMediaURL get the URL of a media from ID
func (wpClient WPClient) GetMediaURL(mediaID int) string {
	ctx := context.Background()
	media, resp, err := wpClient.client.Media.Get(ctx, mediaID, nil)
	defer closeBody(resp)
	if media == nil {
		wordpressClientLogger.WithError(err).WithField("Response", resp).Error("Error while getting media URL")
		return ""
	}

	wordpressClientLogger.WithField("MediaID", mediaID).WithField("MediaURL", media.SourceURL).Debug("Media found")
	return media.SourceURL
}

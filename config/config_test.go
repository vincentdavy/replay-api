package config

import (
	log "github.com/sirupsen/logrus"
	"testing"
)

var configTestLogger = log.WithField("logger", "configTest")

func init() {
	log.SetLevel(log.DebugLevel)
}

func TestNewConfig_NoFile(t *testing.T) {
	config, err := NewConfig("")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_WrongFile(t *testing.T) {
	config, err := NewConfig("config.go")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_GoodFile(t *testing.T) {
	config, err := NewConfig("../config-file/config.yaml")
	if config == nil || err != nil {
		t.Errorf("Config nil or error not nil : %v, %v", config, err)
	}
}

func TestNewConfig_NoPort(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-noPort.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_NoURL(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-noURL.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_WrongPort(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-wrongPort.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_WrongURL(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-wrongURL.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_NoSortingPodcast(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-noSortingPodcast.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_NoSortingSeries(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-noSortingSeries.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_SortingPodcast_WrongOrder(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-sortingPodcastWrongOrder.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_SortingSeries_WrongOrder(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-sortingSeriesWrongOrder.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_SortingPodcast_WrongOrderBy(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-sortingPodcastWrongOrderBy.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_SortingSeries_WrongOrderBy(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-sortingSeriesWrongOrderBy.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_MissingMysql(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-mysql.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

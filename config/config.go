package config

import (
	"errors"
	"fmt"
	"net/url"
	"os"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

const (
	//SortingSeries key for sorting serie parameters
	SortingSeries = "series"
	//SortingPodcast key for sorting podcast parameters
	SortingPodcast = "podcasts"
)

var configLogger = log.WithField("logger", "config")

//Config struct for config data
type Config struct {
	WordpressConfig WordpressConfig `yaml:"wordpress"`
	ServerConfig    ServerConfig    `yaml:"server"`
}

//WordpressConfig struct for wordpress config
type WordpressConfig struct {
	URL               string             `yaml:"url"`
	DefaultPage       int                `yaml:"defaultPage"`
	DefaultPageLength int                `yaml:"defaultPageLength"`
	Sorting           map[string]Sorting `yaml:"sorting,flow"`
	MySQLConnection   string             `yaml:"mysqlURL"`
}

//ServerConfig struct for server config
type ServerConfig struct {
	Port int `yaml:"port"`
}

//Sorting configuration for items sorting
type Sorting struct {
	Order   string `yaml:"order"`
	OrderBy string `yaml:"orderby"`
}

//NewConfig load and parse the config
func NewConfig(configFilePath string) (*Config, error) {
	configLogger.WithField("ConfigFile", configFilePath).Debug("Loading conf file")
	configFile, err := os.Open(configFilePath)
	if err != nil {
		return nil, err
	}

	config := &Config{}
	err = yaml.NewDecoder(configFile).Decode(&config)
	if err != nil {
		return nil, err
	}

	if err = config.checkConfig(); err != nil {
		return nil, err
	}

	configLogger.Debugf("Config values : %+v", config)
	configLogger.Infof("Config loaded")
	return config, nil
}

func (config Config) checkConfig() error {
	if config.ServerConfig.Port <= 0 {
		return errors.New("port must be over 0")
	}

	if _, err := url.ParseRequestURI(config.WordpressConfig.URL); err != nil {
		return fmt.Errorf("invalid server URL : %s", config.WordpressConfig.URL)
	}

	for _, sortingKey := range []string{SortingSeries, SortingPodcast} {
		if err := config.checkSortingConfig(sortingKey); err != nil {
			return err
		}
	}

	if len(config.WordpressConfig.MySQLConnection) == 0 {
		return errors.New("mysql connection URL must be provided")
	}
	return nil
}

func (config Config) checkSortingConfig(key string) error {
	if sorting, ok := config.WordpressConfig.Sorting[key]; ok {
		if err := sorting.checkConfig(); err != nil {
			return fmt.Errorf("error on sorting %s : %s", key, err)
		}
	} else {
		return fmt.Errorf("expecting %s sorting key", key)
	}

	return nil
}

func (sorting Sorting) checkConfig() error {
	if len(sorting.OrderBy) == 0 {
		return errors.New("expecting not empty orderby key")
	}
	if !(sorting.Order == "asc" || sorting.Order == "desc") {
		return errors.New("expecting ASC or DESC order key")
	}

	return nil
}

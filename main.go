package main

import (
	"flag"
	"os"
	"os/signal"
	"replay-api/config"
	"replay-api/items"
	"replay-api/server"

	log "github.com/sirupsen/logrus"
)

const (
	configFileEnvVarName = "CONFIG_FILE"
	debugEnvVarName      = "DEBUG"
)

var (
	version string // set at build time
	commit  string // set at build time
	date    string // set at build time

	configFilePath string // flag
	debugMode      bool   //flag
)

var mainLogger = log.WithField("logger", "main")

func init() {
	flag.StringVar(&configFilePath, "configFile", "", "Config file path (or set via env var CONFIG_FILE)")
	flag.BoolVar(&debugMode, "debug", false, "Enable debug mode (or set env var DEBUG)")
}

func main() {
	mainLogger.Printf("Version : %s - Commit : %s - Date : %s", version, commit, date)
	flag.Parse()

	if debugMode || len(os.Getenv(debugEnvVarName)) > 0 {
		log.SetLevel(log.DebugLevel)
		mainLogger.Debug("Debug mode enabled")
	}

	if len(os.Getenv(configFileEnvVarName)) > 0 {
		configFilePath = os.Getenv(configFileEnvVarName)
	}

	if configFilePath == "" {
		flag.PrintDefaults()
		mainLogger.Fatalln("Missing parameters")
	}

	conf, err := config.NewConfig(configFilePath)
	if err != nil {
		mainLogger.WithFields(log.Fields{
			"Error":          err,
			"ConfigFilePath": configFilePath,
		}).Fatalln("Can't load or parse conf file")
	}

	if err = items.InitDBConn(conf); err != nil {
		mainLogger.WithError(err).Fatal("Error with DB connection")
	}

	exitOnCtrlC()
	if err := server.StartHTTPServer(conf); err != nil {
		mainLogger.WithError(err).Fatalln("Error starting HTTP server")
	}

	mainLogger.Info("Exiting")
}

// ExitOnCtrlC intercepts Ctrl+C signal and exits
func exitOnCtrlC() {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		mainLogger.Info("Interrupt received - exiting")
		items.CloseDBConnection()
		close(c)
		os.Exit(0)
	}()
}

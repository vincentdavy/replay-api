package server

import (
	"fmt"
	"net/http"
	"net/url"
	"replay-api/config"
	"testing"
)

func TestParamIntExtractor_EmptyValue(t *testing.T) {
	paramValue := paramIntExtractor(url.Values{}, "test", 10)
	if paramValue != 10 {
		t.Error("Wrong value received")
	}
}

func TestParamIntExtractor_WrongValueName(t *testing.T) {
	values := url.Values{}
	values.Add("test2", "20")
	paramValue := paramIntExtractor(values, "test", 10)
	if paramValue != 10 {
		t.Error("Wrong value received")
	}
}

func TestParamIntExtractor_WrongString(t *testing.T) {
	values := url.Values{}
	values.Add("test2", "toto")
	paramValue := paramIntExtractor(values, "test", 10)
	if paramValue != 10 {
		t.Error("Wrong value received")
	}
}

func TestParamIntExtractor_GoodCase(t *testing.T) {
	values := url.Values{}
	values.Add("test", "20")
	paramValue := paramIntExtractor(values, "test", 10)
	if paramValue != 20 {
		t.Error("Wrong value received")
	}
}

func TestParamStringExtractor_WrongParamName(t *testing.T) {
	values := url.Values{}
	values.Add("test2", "toto")
	paramValue := paramStringExtractor(values, "test", "default")
	if paramValue != "default" {
		t.Error("Wrong value received")
	}
}

func TestParamStringExtractor_EmptyValue(t *testing.T) {
	values := url.Values{}
	values.Add("test", "")
	paramValue := paramStringExtractor(values, "test", "default")
	if paramValue != "default" {
		t.Error("Wrong value received")
	}
}

func TestParamStringExtractor_GoodCase(t *testing.T) {
	values := url.Values{}
	values.Add("test", "toto")
	paramValue := paramStringExtractor(values, "test", "default")
	if paramValue != "toto" {
		t.Error("Wrong value received")
	}
}

func TestPaginationExtractor_GoodCase(t *testing.T) {
	conf := &config.Config{WordpressConfig: config.WordpressConfig{DefaultPageLength: 10}}
	request := http.Request{URL: &url.URL{RawQuery: fmt.Sprintf("%s=%s&%s=%s&%s=%s&%s=%s", searchParamName, "search", serieIDParamName, "15", pageParamName, "3", pageLengthParamName, "30")}}
	search, serieID, page, pageLength := paramsExtractor(&request, conf)
	if search != "search" || serieID != 15 || page != 3 || pageLength != 30 {
		t.Error("Wrong value received")
	}
}

func TestPaginationExtractor_WrongValueName(t *testing.T) {
	conf := &config.Config{WordpressConfig: config.WordpressConfig{DefaultPageLength: 10}}
	request := http.Request{URL: &url.URL{RawQuery: fmt.Sprintf("%s=%s&%s=%s", "test", "20", pageLengthParamName, "30")}}
	search, serieID, page, pageLength := paramsExtractor(&request, conf)
	if search != "" || serieID != 0 || page != 0 || pageLength != 30 {
		t.Error("Wrong value received")
	}
}

func TestPaginationExtractor_MissingValue(t *testing.T) {
	conf := &config.Config{WordpressConfig: config.WordpressConfig{DefaultPageLength: 00}}
	request := http.Request{URL: &url.URL{RawQuery: fmt.Sprintf("%s=%s", pageParamName, "20")}}
	search, serieID, page, pageLength := paramsExtractor(&request, conf)
	if search != "" || serieID != 0 || page != 20 || pageLength != 0 {
		t.Error("Wrong value received")
	}
}

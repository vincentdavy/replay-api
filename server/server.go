package server

import (
	"encoding/json"
	"net/http"
	"net/url"
	"replay-api/config"
	"replay-api/items"
	"strconv"

	log "github.com/sirupsen/logrus"
)

const (
	apiPrefix                                                             = "/replay-api/v1"
	searchParamName, serieIDParamName, pageParamName, pageLengthParamName = "search", "serie", "page", "per_page"
	defaultSearch                                                         = ""
	defaultSerieID                                                        = 0
	contentType, contentTypeValue                                         = "Content-Type", "application/json; charset=utf-8"
)

var (
	serverLogger = log.WithField("logger", "server")
	headers      = map[string]string{
		contentType: contentTypeValue,
	}
)

//StartHTTPServer starts the HTTP port on specified port and binds all API URL
func StartHTTPServer(config *config.Config) error {
	serverLogger.WithField("Port", config.ServerConfig.Port).Info("Starting HTTP server")
	http.HandleFunc(apiPrefix+"/series", handleSeries(config))
	http.HandleFunc(apiPrefix+"/podcasts", handlePodcasts(config))
	return http.ListenAndServe(":"+strconv.Itoa(config.ServerConfig.Port), nil)
}

func handleSeries(config *config.Config) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		defer request.Body.Close()
		var foundSeries []items.APISerie
		search, _, page, pageLength := paramsExtractor(request, config)
		if search != "" {
			foundSeries = items.SearchSeries(search, page, pageLength, config)
		} else {
			foundSeries = items.GetSeries(page, pageLength, config)
		}
		setHeaders(writer)
		if err := json.NewEncoder(writer).Encode(foundSeries); err != nil {
			serverLogger.WithError(err).Warn("Error with found series serialization")
		}
	}
}

func handlePodcasts(config *config.Config) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		defer request.Body.Close()
		var foundPodcasts []items.APIPodcast
		search, serieID, page, pageLength := paramsExtractor(request, config)
		if serieID != defaultSerieID {
			if search != "" {
				foundPodcasts = items.SearchPodcasts(search, serieID, page, pageLength, config)
			} else {
				foundPodcasts = items.GetPodcasts(serieID, page, pageLength, config)
			}
		}
		setHeaders(writer)
		if err := json.NewEncoder(writer).Encode(foundPodcasts); err != nil {
			serverLogger.WithError(err).Warn("Error with found podcasts serialization")
		}
	}
}

func setHeaders(writer http.ResponseWriter) {
	for header, value := range headers {
		writer.Header().Set(header, value)
	}
}

func paramsExtractor(request *http.Request, config *config.Config) (search string, serieID, page, pageLength int) {
	query := request.URL.Query()
	return paramStringExtractor(query, searchParamName, defaultSearch),
		paramIntExtractor(query, serieIDParamName, defaultSerieID),
		paramIntExtractor(query, pageParamName, config.WordpressConfig.DefaultPage),
		paramIntExtractor(query, pageLengthParamName, config.WordpressConfig.DefaultPageLength)
}

func paramIntExtractor(values url.Values, paramName string, defaultValue int) int {
	if paramStringValue := values.Get(paramName); len(paramStringValue) > 0 {
		if paramIntValue, err := strconv.Atoi(paramStringValue); err == nil {
			return paramIntValue
		}
		return defaultValue
	}
	return defaultValue
}

func paramStringExtractor(values url.Values, paramName string, defaultValue string) string {
	if paramStringValue := values.Get(paramName); len(paramStringValue) > 0 {
		return paramStringValue
	}
	return defaultValue
}

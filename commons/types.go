package commons

//SerieType the serie type in wordpress
type SerieType struct {
	ID          int      `json:"id"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Slug        string   `json:"slug"`
	Parent      int      `json:"parent"`
	Meta        []string `json:"meta"`
	Count       int      `json:"count"`
}

//PodcastType the podcast type in wordpress
type PodcastType struct {
	ID    int `json:"id,omitempty"`
	Title struct {
		Rendered string `json:"rendered"`
	} `json:"title"`
	Serie         []int  `json:"series"`
	FeaturedMedia int    `json:"featured_media"`
	Slug          string `json:"slug"`
	Status        string `json:"status"`
	Date          string `json:"date"`
	Meta          struct {
		EpisodeType string `json:"episode_type"`
		AudioFile   string `json:"audio_file"`
		FileSize    string `json:"filesize_raw"`
		Duration    string `json:"duration"`
	} `json:"meta"`
}

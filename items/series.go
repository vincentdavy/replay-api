package items

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql" //mysql driver
	log "github.com/sirupsen/logrus"
	"replay-api/commons"
	"replay-api/config"
	"replay-api/wordpress"
)

const (
	mysqlDriverName = "mysql"
	sqlQuery        = `SELECT meta_value FROM wp_termmeta 
		INNER JOIN wp_terms ON wp_termmeta.term_id = wp_terms.term_id
		WHERE wp_termmeta.meta_key = "podcast_series_image_settings" 
		AND wp_terms.term_id = ?`
)

var (
	db   *sql.DB
	stmt *sql.Stmt
)

//APISerie represents series for API side
type APISerie struct {
	ID       int    `json:"id"`
	Title    string `json:"title"`
	ImageURL string `json:"imageURL,omitempty"`
	Count    int    `json:"count"`
}

var seriesLogger = log.WithField("logger", "series")

//InitDBConn initialize the DB connection at startup
func InitDBConn(config *config.Config) error {
	var err error
	db, err = sql.Open(mysqlDriverName, config.WordpressConfig.MySQLConnection)
	if err != nil {
		return err
	}

	// testing connection
	if err = db.Ping(); err != nil {
		return err
	}

	// preparing query statement
	stmt, err = db.Prepare(sqlQuery)
	if err != nil {
		return err
	}

	seriesLogger.Info("DB connection opened")
	return nil
}

//CloseDBConnection close DB connection when ending
func CloseDBConnection() {
	stmt.Close()
	db.Close()
	seriesLogger.Info("DB connection closed")
}

//SearchSeries search podcast series
func SearchSeries(search string, page, pageLength int, config *config.Config) []APISerie {
	seriesLogger.WithField("Search", search).Debugln("All series search")
	var series []APISerie
	if wpClient, err := wordpress.NewWordpressClient(config); err == nil {
		series = loadSeries(wpClient, func() []commons.SerieType {
			return wpClient.LoadSeries(search, page, pageLength)
		})
	} else {
		seriesLogger.WithError(err).Error("Error creating Wordpress client")
	}
	seriesLogger.WithField("Search", search).Debugf("Search series loaded : %+v", series)
	return series
}

//GetSeries returns all podcast series
func GetSeries(page, pageLength int, config *config.Config) []APISerie {
	seriesLogger.Debugln("All series request")
	var series []APISerie
	if wpClient, err := wordpress.NewWordpressClient(config); err == nil {
		series = loadSeries(wpClient, func() []commons.SerieType {
			return wpClient.LoadSeries("", page, pageLength)
		})
	} else {
		seriesLogger.WithError(err).Error("Error creating Wordpress client")
	}
	seriesLogger.Debugf("All series loaded : %+v", series)
	return series
}

func loadSeries(wpClient *wordpress.WPClient, seriesFunc func() []commons.SerieType) []APISerie {

	series := make([]APISerie, 0)

	for _, serie := range seriesFunc() {
		apiSerie := APISerie{
			ID:    serie.ID,
			Title: serie.Name,
			Count: serie.Count,
		}

		if mediaID := getSerieMediaID(serie); mediaID != 0 {
			mediaURL := wpClient.GetMediaURL(mediaID)
			if len(mediaURL) > 0 {
				apiSerie.ImageURL = mediaURL
			}
		}
		series = append(series, apiSerie)
	}

	return series
}

func getSerieMediaID(serie commons.SerieType) int {
	seriesLogger.WithField("SerieID", serie.ID).Debug("Searching serie media ID")

	var mediaID int
	err := stmt.QueryRow(serie.ID).Scan(&mediaID)
	if err != nil {
		switch err.Error() {
		case "sql: no rows in result set":
			seriesLogger.WithField("SerieID", serie.ID).Debug("No media found")
			return 0
		default:
			seriesLogger.WithError(err).Warn("Error with SQL query")
			return 0
		}
	}

	seriesLogger.WithField("SerieID", serie.ID).WithField("MediaID", mediaID).Debug("Serie media ID found")
	return mediaID

}

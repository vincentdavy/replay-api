package items

import (
	"replay-api/commons"
	"replay-api/config"
	"replay-api/wordpress"
	"strconv"
	"strings"
	"time"
)

const (
	dateFormat    = "2006-01-02T15:04:05"
	dateSeparator = ":"
)

//APIPodcast represents podcasts for API side
type APIPodcast struct {
	ID       int           `json:"id"`
	Title    string        `json:"title"`
	ImageURL string        `json:"imageURL,omitempty"`
	Date     time.Time     `json:"date"`
	Duration time.Duration `json:"duration"`
	FileSize int           `json:"fileSize"`
	FileURL  string        `json:"fileURL"`
}

//SearchPodcasts search podcasts in a serie
func SearchPodcasts(search string, serieID, page, pageLength int, config *config.Config) []APIPodcast {
	seriesLogger.WithField("Search", search).Debugln("Podcasts search")
	var podcasts []APIPodcast
	if wpClient, err := wordpress.NewWordpressClient(config); err == nil {
		podcasts = loadPodcasts(wpClient, func() []commons.PodcastType {
			return wpClient.LoadPodcasts(search, serieID, page, pageLength)
		})
	} else {
		seriesLogger.WithError(err).Error("Error creating Wordpress client")
	}
	seriesLogger.WithField("Search", search).Debugf("Search podcasts loaded : %+v", podcasts)
	return podcasts
}

//GetPodcasts returns all podcast for a serie
func GetPodcasts(serieID, page, pageLength int, config *config.Config) []APIPodcast {
	seriesLogger.Debugln("Podcasts request")
	var podcasts []APIPodcast
	if wpClient, err := wordpress.NewWordpressClient(config); err == nil {
		podcasts = loadPodcasts(wpClient, func() []commons.PodcastType {
			return wpClient.LoadPodcasts("", serieID, page, pageLength)
		})
	} else {
		seriesLogger.WithError(err).Error("Error creating Wordpress client")
	}
	seriesLogger.Debugf("Podcasts loaded : %+v", podcasts)
	return podcasts
}

func loadPodcasts(wpClient *wordpress.WPClient, podcastsFunc func() []commons.PodcastType) []APIPodcast {

	podcasts := make([]APIPodcast, 0)

	for _, podcast := range podcastsFunc() {
		apiPodcast := APIPodcast{
			ID:      podcast.ID,
			Title:   podcast.Title.Rendered,
			FileURL: podcast.Meta.AudioFile,
		}

		if fileSize, err := strconv.Atoi(podcast.Meta.FileSize); err == nil {
			apiPodcast.FileSize = fileSize
		}

		if date, err := time.Parse(dateFormat, podcast.Date); err == nil {
			apiPodcast.Date = date
		}

		if len(podcast.Meta.Duration) > 0 {
			apiPodcast.Duration = extractPodcastDuration(podcast.Meta.Duration)
		}

		mediaURL := wpClient.GetMediaURL(podcast.FeaturedMedia)
		if len(mediaURL) > 0 {
			apiPodcast.ImageURL = mediaURL
		}

		podcasts = append(podcasts, apiPodcast)
	}

	return podcasts
}

func extractPodcastDuration(durationAsString string) time.Duration {
	splits := strings.Split(durationAsString, dateSeparator)
	if !(len(splits) == 1 && splits[0] == durationAsString) {
		var length time.Duration
		if len(splits) >= 1 {
			length += addDuration(splits[len(splits)-1], time.Second)
		}
		if len(splits) >= 2 {
			length += addDuration(splits[len(splits)-2], time.Minute)
		}
		if len(splits) >= 3 {
			length += addDuration(splits[len(splits)-3], time.Hour)
		}

		return length
	}

	return 0
}

func addDuration(value string, baseDuration time.Duration) time.Duration {
	if dur, err := strconv.Atoi(value); err == nil {
		return time.Duration(dur) * baseDuration
	}

	return 0
}

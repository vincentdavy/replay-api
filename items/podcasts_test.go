package items

import (
	"testing"
	"time"
)

func TestAddDuration(t *testing.T) {
	dur := addDuration("10", time.Second)
	if dur != 10*time.Second {
		t.Errorf("Wrong time received : %v", dur)
	}
}

func TestExtractPodcastDuration_Global(t *testing.T) {
	dur := extractPodcastDuration("1:1:1")
	if dur != time.Hour+time.Minute+time.Second {
		t.Error("Wrong time received")
	}
}

func TestExtractPodcastDuration_Minute(t *testing.T) {
	dur := extractPodcastDuration("1:1")
	if dur != time.Minute+time.Second {
		t.Errorf("Wrong time received : %v", dur)
	}
}

func TestExtractPodcastDuration_Second(t *testing.T) {
	dur := extractPodcastDuration("0:1")
	if dur != time.Second {
		t.Errorf("Wrong time received : %v", dur)
	}
}

func TestExtractPodcastDuration_2140(t *testing.T) {
	dur := extractPodcastDuration("21:40")
	if dur != time.Minute*21+time.Second*40 {
		t.Error("Wrong time received")
	}
}
